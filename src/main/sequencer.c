#include "sequencer.h"
#include <stddef.h>

typedef void (*funcType1)(int, int, int);
typedef void (*funcType2)(int);
typedef void (*funcType3)(int, int, int, char*);
typedef char* (*funcType4)();
typedef int (*funcType5)();
typedef char (*funcType6)(void);
typedef void (*funcType7)();
funcType1 midiWrite;
funcType2 delayMillis;
funcType3 lcdPrint;
funcType4 getKey;
funcType5 getMillis;
funcType5 getDistance;
funcType5 getLight;
funcType6 waitForKeypad;
funcType7 changeMode;

int lightValueMiddle;

char* getNoteName(int number);

int currentMode; // 0 = menu, 1 = move through layers, 2 = select note, 3 = select note with laser, 4 = select BPM, 5 = load preset
int currentMenu; // 0 = move through layers, 1 = select note, 2 = select note with laser, 3 = select BPM, 4 = load preset

int currentLayer;
int currentPos;
int currentOctave;
int currentHalftone; // relative
int currentTone; // absolute
int currentPresetMenu;
const int numberOfPresets = 4;

int steps[4][16];
float bpm = 120.0;
float interval;

int inSequencer;

char presetName[5][16] = {"     beat01     ", "     beat02     ", "In the Air Drums", "In the Air Chrds", "Stranger Things "};
int beat01[4][16] = { {NULL, 60, NULL, 65, NULL, 68, 70, NULL, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                      {NULL, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                      {NULL, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                      {NULL, NULL, NULL, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};

int beat02[4][16] = { {70, 60, NULL, 65, NULL, 68, 70, NULL, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                      {NULL, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                      {NULL, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                      {NULL, NULL, NULL, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};

int inTheAirDrums[4][16] = { {44  , NULL, 43  , NULL, NULL, NULL, 43  , NULL, NULL, NULL, 43  , NULL, 44  , NULL, 43  , NULL}, //hihats
                        {NULL, NULL, 42  , NULL, NULL, NULL, 42  , NULL, NULL, NULL, NULL, NULL, 37  , NULL, NULL, NULL}, //snare / toms
                        {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36  , NULL, NULL, NULL, NULL, NULL}, //base
                        {NULL, NULL, NULL, NULL, 40  , NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40  , NULL, NULL, NULL}};  //accent 

int inTheAirChords[4][16] = { {62  , NULL, NULL, NULL, 60  , NULL, NULL, NULL, 58  , NULL, NULL, NULL, 60  , NULL, NULL, NULL},
                              {65  , NULL, NULL, NULL, 64  , NULL, NULL, NULL, 62  , NULL, NULL, NULL, 64  , NULL, NULL, NULL},
                              {69  , NULL, NULL, NULL, 67  , NULL, NULL, NULL, 65  , NULL, NULL, NULL, 67  , NULL, NULL, NULL},
                              {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};

int strangerThings[4][16] = { {0x3c, 0x40, 0x43, 0x47, 0x48, 0x47, 0x43, 0x40, 0x3c, 0x40, 0x43, 0x47, 0x48, 0x47, 0x43, 0x40},
                              {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                              {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                              {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};

/* for copy-pasting
int name[4][16] = { {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                        {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                        {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
                        {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};
*/                

void initSequencer(void (*f)(int, int, int), void (*g)(int), void (*h)(int, int, int, char*), char* (*i)(), int (*j)(), char (*k)(void), int (*l)(), int (*m)(), void (*n)()) {

  clearSequencer();
  midiWrite = f;
  delayMillis = g;
  lcdPrint = h;
  getKey = i;
  getMillis = j;
  waitForKeypad = k;
  getDistance = l;
  getLight = m;
  changeMode = n;

  inSequencer = 0;

  lightValueMiddle = 200;

  currentLayer = 0;
  currentPos = 0;
  currentOctave = 6; // TODO: maybe change if not good starting octave
  currentHalftone = 0;
  currentTone = 60;
  currentMode = 1;
  currentMenu = 0;
  currentPresetMenu = 0;

  calculateInterval();
}

void returnToSequencer() {
  inSequencer = 1;

  currentLayer = 0;
  currentPos = 0;
  currentMode = 0;
  currentHalftone = 0;
  currentTone = 60;
  currentMenu = 0;
  currentPresetMenu = 0;

  getLightValueMiddle();

  printMenu();
}

void switchToLive() {
  currentMenu = 0;
  currentMode = 0;
  inSequencer = 0;
  changeMode();
}

void handleSeqKeyInput(char key) {
  switch (key) {
      case '1':
        if (currentMode == 1) {
          currentMode = 2; // select note
          printNote();
        }
        break;
      case '2':
        if (currentMode == 1) {
          currentMode = 3; // select note laser
          selectNoteLaser();
        }
        break;
      case '3':
        break;
      case '4':
        break;
      case '5':
        break;
      case '6':
        break;
      case 'B':
        currentMode = 0;
        currentMenu = 0;
        printMenu();
        break;
      case '7':
        break;
      case '8':
        break;
      case '9':
        break;
      case 'C':
        playSequencer();
        break;
      case '*':
        if (currentMode == 0) {
          currentMenu = (currentMenu + 4) % 5;
          printMenu();
        } else if (currentMode == 1) {
          currentPos = (currentPos + 15) % 16;
          printPos();
        } else if (currentMode == 2 && currentTone > 0) {
          currentTone -= 1;
          currentOctave = (currentTone / 12) - 1;
          currentHalftone = currentTone % 12;
          showNote();
        } else if (currentMode == 3 && currentOctave > 0) {
          currentOctave -= 1;
        } else if (currentMode == 5) {
          currentPresetMenu = (currentPresetMenu + (numberOfPresets - 1)) % numberOfPresets;
          printPresetMenu();
        }
        break;
      case '0':
        if (currentMode == 0) {
          currentMode = currentMenu + 1;
          if (currentMenu == 0) {
            printLayerChange();
          } else if (currentMenu == 1) {
            printNote();
          } else if (currentMenu == 2) {
            selectNoteLaser();
          } else if (currentMenu == 3) {
            setBpm();
          } else if (currentMenu == 4) {
            printPresetMenu();
          }
        } else if (currentMode == 1) {
          setNote();
        } else if (currentMode == 2) {
          currentMode = 1;
          printLayer();
          printPos();
        } else if (currentMode == 5) {
          currentMode = 1;
          loadPreset();
          printLayer();
          printPos();
        }
        break;
      case '#':
        if (currentMode == 0) {
          currentMenu = (currentMenu + 1) % 5;
          printMenu();
        } else if (currentMode == 1) {
          currentPos = (currentPos + 1) % 16;
          printPos();
        } else if (currentMode == 2 && currentTone < 127) {
          currentTone += 1;
          currentOctave = (currentTone / 12) - 1;
          currentHalftone = currentTone % 12;
          showNote();
        } else if (currentMode == 3 && currentOctave < 8) {
          currentOctave += 1;
        } else if (currentMode == 5) {
          currentPresetMenu = (currentPresetMenu + 1) % numberOfPresets;
          printPresetMenu();
        }
        break;
      case 'D':
        if (currentMode == 1) {
          nextLayer();
        } else {
          currentMode = 1;
          printLayerChange();
        }
        break;
    }
}

////////////////////////// B selected -> menu //////////////////////////

void printMenu() {
  lcdPrint(2, 0, 0, '0'); // clears LCD
  lcdPrint(0, 0, 16, " Sequencer Menu:");
  switch (currentMenu) {
    case 0:
      lcdPrint(1, 0, 7, " Layers");
      break;
    case 1:
      lcdPrint(1, 0, 12, " Select Tone");
      break;
    case 2:
      lcdPrint(1, 0, 16, " Select w. Laser");
      break;
    case 3:
      lcdPrint(1, 0, 11, " Select BPM");
      break;
    case 4:
      lcdPrint(1, 0, 14, " Select Preset");
      break;
  }
}

void playSequencer() {
  //check if time frame has passed
  currentPos = 0;
  long startTime;
  while (1) {
    if (getKey() != NULL) {
      break;
    }
    //write midi note from sequence array
    startTime = getMillis();
    if (steps[0][currentPos] != NULL) {
      midiWrite(0x90, steps[0][currentPos], 0x45);
    }
    if (steps[1][currentPos] != NULL) {
      midiWrite(0x90, steps[1][currentPos], 0x45);
    }
    if (steps[2][currentPos] != NULL) {
      midiWrite(0x90, steps[2][currentPos], 0x45);
    }
    if (steps[3][currentPos] != NULL) {
      midiWrite(0x90, steps[3][currentPos], 0x45);
    }
    delayMillis((int) (interval / 4) - (getMillis() - startTime));
    midiWrite(0x80, steps[0][currentPos], 0x00);
    midiWrite(0x80, steps[1][currentPos], 0x00);
    midiWrite(0x80, steps[2][currentPos], 0x00);
    midiWrite(0x80, steps[3][currentPos], 0x00);

    //if end of array is reached
    currentPos = (currentPos + 1) % 16;
    printPos();
  }

}

// prints and plays note
void showNote() {
  lcdPrint(2, 0, 0, '0'); // clears LCD
  printNote();
  playNote();
}

/**
 * clears the whole array
*/
void clearSequencer() {
  int i, j;
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 16; j++) {
      steps[i][j] = NULL;
    }
  }
  return;
}

void printNote() {
  lcdPrint(2, 0, 0, '0'); // clears LCD
  lcdPrint(0, 0, 9, " Note:   ");
  lcdPrint(0, 10, 2, getNoteName(currentTone));
  lcdPrint(1, 0, 9, " Octave: ");
  int oct = currentTone / 12;
  char str3 = '0' + oct - 1;
  lcdPrint(1, 10, 1, &str3);
}

void playNote() {
  midiWrite(0x90, currentTone, 0x41);
  delayMillis(100);
  midiWrite(0x80, currentTone, 0x00);
}

/**
 * Octaves only from 1 to 8 (4 = default)
 **/
void selectNoteLaser() {

  int maxDistance = 40;

  lcdPrint(2, 0, 0, '0');
  lcdPrint(0, 0, 9, " Note:   ");
  lcdPrint(0, 10, 2, getNoteName(currentHalftone));
  lcdPrint(1, 0, 9, " Octave: ");
  char c1 = '0' + currentOctave;
  lcdPrint(1, 10, 1, &c1);
  
  char key;
  int isPressed = 0;
  int tempDistance;
  while (1) {
    key = getKey();
    if (key == '*') {
      if (currentOctave > 1) {
        currentOctave -= 1;
        char c1 = '0' + currentOctave;
        lcdPrint(1, 10, 1, &c1);
      }
    } else if (key == '#') {
      if (currentOctave < 8) {
        currentOctave += 1;
        char c1 = '0' + currentOctave;
        lcdPrint(1, 10, 1, &c1);
      }
    } else if (key == 'B') {
      printMenu();
      break;
    } else if (key == '0') {
      // go back to layer
      currentMode = 1;
      printLayer();
      printPos();
      break;
    } else if (key == 'A') {
      switchToLive();
      break;
    }
    tempDistance = getDistance();
    if (tempDistance > maxDistance) {
      // do nothing
    } else if (getLight() > lightValueMiddle && isPressed == 0) {
      currentHalftone = getToneFromDistance(tempDistance);
      isPressed = 1;
      currentTone = (currentOctave + 1) * 12 + currentHalftone;
      midiWrite(0x90, currentTone, 0x45);
      lcdPrint(0, 10, 2, getNoteName(currentHalftone));
    }
    if (getLight() < lightValueMiddle) {
      isPressed = 0;
      midiWrite(0x80, currentTone, 0x00);
    }
    delayMillis(10);
  }
  if (isPressed) {
    midiWrite(0x80, currentTone, 0x00);
  }
}

// put current note in step array (or remove it if already there)
void setNote() { 
  if (steps[currentLayer][currentPos] != currentTone) {
    steps[currentLayer][currentPos] = currentTone;
  } else {
    steps[currentLayer][currentPos] = NULL;
  }
  playNote();
  printLayer();
}

void nextLayer() {
  currentLayer = (currentLayer + 1) % 4;
  printLayerChange();
}

void printLayerChange() {

  lcdPrint(2, 0, 0, '0'); // clears LCD
  char str2[] = " Layer: ";
  lcdPrint(0, 0, 8, str2);
  char str3 = '1' + currentLayer;
  lcdPrint(0, 8, 1, &str3);

  delayMillis(500); // short time buffer

  printLayer();
  printPos();
}

void printLayer() {
  char str[] = "                ";
  lcdPrint(0, 0, 16, str); // clears top row LCD
  char notePos[16];
  int i;
  for (i = 0; i < 16; i++) {
    if (steps[currentLayer][i] == NULL) {
      notePos[i] = ' ';
    } else {
      notePos[i] = 'X';
    }
  }
  lcdPrint(0, 0, 16, notePos);
}

void setBpm() {

  lcdPrint(2, 0, 0, '0'); // clears LCD
  lcdPrint(0, 0, 12, " Enter BPM: ");
  
  int a, b, c;

  a = waitForKeypad() - '0';
  if (a > 9 || a < 0) {
    a = 1;
  }
  char ca = '0' + a;
  lcdPrint(0, 12, 1, &ca);
  b = waitForKeypad() - '0';
  if (b > 9 || b < 0) {
    b = 1;
  }
  char cb = '0' + b;
  lcdPrint(0, 13, 1, &cb);
  c = waitForKeypad() - '0';
  if (c > 9 || c < 0) {
    c = 1;
  }
  char cc = '0' + c;
  lcdPrint(0, 14, 1, &cc);

  bpm = a * 100 + b * 10 + c;
  calculateInterval();
  return;
}

void calculateInterval() {
  // based on bpm, in millis
  if (bpm < 20) {
    bpm = 20;
  } else if (bpm > 240) {
    bpm = 240;
  }
  interval = (60.0 / bpm) * 1000;
  return;
}

void printPresetMenu() {
  lcdPrint(2, 0, 0, '0');
  lcdPrint(0, 0, 16, presetName[currentPresetMenu]);
}

void loadPreset() {
  int* presetToLoad;
  switch (currentPresetMenu) {
    case 0:
      presetToLoad = beat01;
      break;
    case 1:
      presetToLoad = beat02;
      break;
    case 2:
      presetToLoad = inTheAirDrums;
      bpm = 102;
      interval = (60.0 / bpm) * 1000;
      break;
    case 3:
      presetToLoad = inTheAirChords;
      bpm = 24;
      interval = (60.0 / bpm) * 1000;
      break;
    case 4:
      presetToLoad = strangerThings;
      break;
  }
  int layer;
  int pos;
  for (layer = 0; layer < 4; layer++) {
    for (pos = 0; pos < 16; pos++) {
      steps[layer][pos] = presetToLoad[(layer * 16) + pos];
    }
  }
}

void printPos() {
  char str[] = "                ";
  lcdPrint(1, 0, 16, str); // clears bottom row LCD
  char marker = '^';
  lcdPrint(1, currentPos, 1, &marker); // prints position marker
}

char* getNoteName(int number) {
  int note = number % 12;
  char str[2];
  switch (note) {
    case 0:
      return "C ";
      case 1:
      return "C#";
      case 2:
      return "D ";
      case 3:
      return "D#";
      case 4:
      return "E ";
      case 5:
      return "F ";
      case 6:
      return "F#";
      case 7:
      return "G ";
      case 8:
      return "G#";
      case 9:
      return "A ";
      case 10:
      return "A#";
      case 11:
      return "B ";
  }
}
