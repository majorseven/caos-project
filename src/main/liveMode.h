void initLiveMode(void (*f)(int, int, int), void (*g)(int), void (*h)(int, int, int, char*), char* (*i)(), int (*j)(), char (*k)(void), int (*l)(), int (*m)(), void (*n)());
void returnToLiveMode();
void handleLiveKeyInput(char input);
void play(int sound, int distance);
int hasToPlay(int lightVal);
