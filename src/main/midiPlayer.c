#include "midiPlayer.h"
#include <stddef.h>

typedef void (*funcType1)(int, int, int);
funcType1 midiWrite;

const int velocity = 0x45;

int currentNotes[5]; //array of maxium amount of notes, index 0 is always root note
int currentNumNotes;
int currentIsMajor;

void initMidiPlayer(void (*f)(int, int, int)) {
    midiWrite = f;
    int i;
    for (i = 0; i < 5; i++) currentNotes[i] = -1;
}

/**
 * note: from 0 to 127, is base of chord
 * numNotes: from 0 to 5 - 0 clears previous input
 * isMajor: 1 Major, 0 Minor
 */
void playMidi(int note, int numNotes, int isMajor /*maybe add possibility for inversions*/) {
    //check if note in correct range
    if (note < 0) note = 0;
    if (note > 127) note = 127;

    currentNotes[0] = note;  //fill in root note
    currentNumNotes = numNotes;
    currentIsMajor = isMajor;

    switch (numNotes) {
    case 0:
        clearMidi();
        break;

    case 1:
        playSingleNote();
        break;

    case 2:
        playTwoNotes();
        break;

    case 3:
        playThreeNotes();
        break;

    case 4:
        playFourNotes();
        break;

    case 5:
        playFiveNotes();
        break;
    }
}

void playMidiScales(int note, int numNotes, int isMajor, int baseKey) {
    //check if note in correct range
    if (note < 0) note = 0;
    if (note > 127) note = 127;

    currentNumNotes = numNotes;
    currentIsMajor = isMajor;
    currentNotes[0] = note;

    switch (numNotes) {
    case 0:
        clearMidi();
        break;

    case 1:
        playSingleNote();
        break;

    case 2:
        playTwoNotesScales(baseKey);
        break;

    case 3:
        playThreeNotesScales(baseKey);
        break;

    case 4:
        playFourNotesScales(baseKey);
        break;

    case 5:
        playFiveNotesScales(baseKey);
        break;
    } 
}

void clearMidi() {
    cancelCurrentNotes();
    currentIsMajor = -1;
    currentNumNotes = -1;
}

void playCurrentNotes() {
    int i;
    for (i = 0; i < 5; i++) {
        int note = currentNotes[i];
        if (note != -1) {
            midiWrite(0x90, note, velocity);
        }
    }
}

void cancelCurrentNotes() {
    //cancel base
    midiWrite(0x90, currentNotes[0] - 12, 0x00);
    //cancel chord
    int i;
    for (i = 0; i < 5; i++) {
        int note = currentNotes[i];
        if (note != -1) {
            midiWrite(0x90, note, 0x00);
            currentNotes[i] = -1;
        }
    }
}

void playSingleNote() {
    midiWrite(0x90, currentNotes[0], velocity);
}

void playTwoNotes() {
    if (currentIsMajor) currentNotes[1] = currentNotes[0] + 4;
    else currentNotes[1] = currentNotes[0] + 3;
    playCurrentNotes();
}

void playThreeNotes() {
    if (currentIsMajor) {
        //eg Cmaj
        currentNotes[1] = currentNotes[0] + 4;
        currentNotes[2] = currentNotes[0] + 7;
    } else {
        //eg Cm
        currentNotes[1] = currentNotes[0] + 3;
        currentNotes[2] = currentNotes[0] + 7;
    }
    playCurrentNotes();
}

void playFourNotes() {
    if (currentIsMajor) {
        //eg Cmaj7
        currentNotes[1] = currentNotes[0] + 4;
        currentNotes[2] = currentNotes[0] + 7;
        currentNotes[3] = currentNotes[0] + 11;
    } else {
        //eg Cm7
        currentNotes[1] = currentNotes[0] + 3;
        currentNotes[2] = currentNotes[0] + 7;
        currentNotes[3] = currentNotes[0] + 10;
    }
    playCurrentNotes();
}

void playFiveNotes() {
    if (currentIsMajor) {
        //eg Cmaj9
        currentNotes[1] = currentNotes[0] + 4;
        currentNotes[2] = currentNotes[0] + 7;
        currentNotes[3] = currentNotes[0] + 11;
        currentNotes[4] = currentNotes[0] + 14;
    } else {
        //eg Cm9
        currentNotes[1] = currentNotes[0] + 3;
        currentNotes[2] = currentNotes[0] + 7;
        currentNotes[3] = currentNotes[0] + 10;
        currentNotes[5] = currentNotes[0] + 14;
    }
    playCurrentNotes();
}

void playTwoNotesScales(int baseKey) {
    currentNotes[1] = baseKey + getChordInterval(baseKey, currentNotes[0], 1, currentIsMajor);
    playCurrentNotes();
}

void playThreeNotesScales(int baseKey) {
    currentNotes[1] = baseKey + getChordInterval(baseKey, currentNotes[0], 1, currentIsMajor);
    currentNotes[2] = baseKey + getChordInterval(baseKey, currentNotes[0], 2, currentIsMajor);
    playCurrentNotes();
}

void playFourNotesScales(int baseKey) {
    currentNotes[1] = baseKey + getChordInterval(baseKey, currentNotes[0], 1, currentIsMajor);
    currentNotes[2] = baseKey + getChordInterval(baseKey, currentNotes[0], 2, currentIsMajor);
    currentNotes[3] = baseKey + getChordInterval(baseKey, currentNotes[0], 3, currentIsMajor);
    playCurrentNotes();
}

void playFiveNotesScales(int baseKey) {
    currentNotes[1] = baseKey + getChordInterval(baseKey, currentNotes[0], 1, currentIsMajor);
    currentNotes[2] = baseKey + getChordInterval(baseKey, currentNotes[0], 2, currentIsMajor);
    currentNotes[3] = baseKey + getChordInterval(baseKey, currentNotes[0], 3, currentIsMajor);
    currentNotes[4] = baseKey + getChordInterval(baseKey, currentNotes[0], 4, currentIsMajor);
    playCurrentNotes();
}

void playBaseTone() {
    if (currentNotes[0] != -1) midiWrite(0x90, currentNotes[0] - 12, velocity);
}
