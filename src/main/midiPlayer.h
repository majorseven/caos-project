void initMidiPlayer(void (*f)(int, int, int));
void playMidi(int note, int numNotes, int isMajor);
void clearMidi();