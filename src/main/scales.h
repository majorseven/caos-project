const int maj[7][5] = {
    {0, 4, 7, 11, 14},
    {2, 5, 9, 12, 16},
    {4, 7, 11, 14, 19},
    {5, 9, 12, 16, 19},
    {7, 11, 14, 16, 21},
    {9, 12, 14, 19, 23},
    {11, 14, 16, 19, 21}
};

const int min[7][5] = {
    {0, 3, 7, 10, 14},
    {2, 5, 9, 12, 17},
    {3, 7, 10, 14, 17},
    {5, 8, 10, 15, 19},
    {7, 10, 12, 14, 17},
    {8, 12, 15, 19, 22},
    {10, 14, 15, 17, 22},
};

int getMajTone(int base, int step) {
    switch (step) {
        case 0:
        return base;
        break;

        case 1:
        return base + 2;
        break;

        case 2:
        return base + 4;
        break;

        case 3:
        return base + 5;
        break;

        case 4:
        return base + 7;
        break;

        case 5:
        return base + 9;
        break;

        case 6:
        return base + 11;
        break;

        case 7:
        return base + 12;
        break;
    }
}

int getMinTone(int base, int step) {
    switch (step) {
        case 0:
        return base;
        break;

        case 1:
        return base + 2;
        break;

        case 2:
        return base + 3;
        break;

        case 3:
        return base + 5;
        break;

        case 4:
        return base + 7;
        break;

        case 5:
        return base + 8;
        break;

        case 6:
        return base + 10;
        break;

        case 7:
        return base + 12;
        break;
    }
}

int getScaleTone(int isMajor, int base, int step) {
    if (isMajor) return getMajTone(base, step);
    else return getMinTone(base, step);
}

char* getKeyFromBase(int base) {
    while (base < 48) base += 12;
    while (base > 59) base -= 12;

    switch (base)
    {
    case 48:
        return " C";
        break;

    case 49:
    return "C#";
        break;

    case 50:
        return " D";
        break;

    case 51:
        return "D#";
        break;

    case 52:
        return " E";
        break;

    case 53:
        return " F";
        break;

    case 54:
        return "F#";
        break;

    case 55:
        return " G";
        break;

    case 56:
        return "G#";
        break;

    case 57:
        return " A";
        break;

    case 58:
        return "A#";
        break;

    case 59:
        return " H";
        break;
    }
}

int getChordInterval(int base, int note, int num, int isMajor) {
    if (isMajor) return maj[(note - base + 1) / 2][num];
    else return min[(note - base) * 2 / 3][num];
}
