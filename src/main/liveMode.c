#include "liveMode.h"
#include <stddef.h>

typedef void (*funcType1)(int, int, int);
typedef void (*funcType2)(int);
typedef void (*funcType3)(int, int, int, char*);
typedef char* (*funcType4)();
typedef int (*funcType5)();
typedef char (*funcType6)(void);
typedef void (*funcType7)();
funcType1 midiWrite;
funcType2 delayMillis;
funcType3 lcdPrint;
funcType4 getKey;
funcType5 getMillis;
funcType6 waitForKeypad;
funcType5 getDistance;
funcType5 getLight;
funcType7 changeMode;

const int maxDistance = 40;

int lightValueMiddle;

int lightValBefore = 0;
int currentMode; // 0 = menu, 1 = adjust light, 2 = normal, 3 = normal+, 4 = pitch, 5 = solo, 6 = scales
int currentMenu; // 0 = adjust light, 1 = normal, 2 = normal+, 3 = pitch, 4 = voicings

int octave;
int halftone;

int inLiveMode;

void initLiveMode(void (*f)(int, int, int), void (*g)(int), void (*h)(int, int, int, char*), char* (*i)(), int (*j)(), char (*k)(void), int (*l)(), int (*m)(), void (*n)()) {
    midiWrite = f;
    delayMillis = g;
    lcdPrint = h;
    getKey = i;
    getMillis = j;
    waitForKeypad = k;
    getDistance = l;
    getLight = m;
    changeMode = n;

    inLiveMode = 1;

    lightValueMiddle = 200;
    currentMenu = 0;
    currentMode = 1;
    adjustLight();

    octave = 5; // default value
    halftone = 0; // default value
}

void handleLiveKeyInput(char key) {
  switch (key) {
    case 'B':
      currentMode = 0;
      currentMenu = 0;
      printLiveMenu();
      break;
    case '*':
      if (currentMode == 0) {
        currentMenu = (currentMenu + 5) % 6;
        printLiveMenu();
      }
      break;
    case '#':
      if (currentMode == 0) {
        currentMenu = (currentMenu + 1) % 6;
        printLiveMenu();
      }
      break;
    case '0':
      if (currentMode == 0) {
        if (currentMenu == 0) {
          currentMode = currentMenu + 1;
          adjustLight();
        } else if (currentMenu == 1) {
          currentMode = currentMenu + 1;
          playNormal();
        } else if (currentMenu == 2) {
          currentMode = currentMenu + 1;
          playNormalPlus();
        } else if (currentMenu == 3) {
          currentMode = currentMenu + 1;
          playPitch();
        } else if (currentMenu == 4) {
          currentMode = currentMenu + 1;
          playSolo();
        } else if (currentMenu == 5) {
          currentMode = currentMenu + 1;
          playScales();
        }
      }
      break;
  }
}

void returnToLiveMode() {
  inLiveMode = 1;
  currentMode = 0;
  currentMenu = 0;
  printLiveMenu();
}

void switchToSeq() {
  currentMenu = 0;
  currentMode = 0;
  inLiveMode = 0;
  changeMode();
}

void printLiveMenu() {
  if (!inLiveMode) return;

  lcdPrint(2, 0, 0, '0'); // clears LCD
  char menu[] = " Live Menu:";
  lcdPrint(0, 0, 11, menu);
  char str[] = " Adjust Light";
  char str2[] = " Normal Mode";
  char str3[] = " Normal Mode+";
  char str4[] = " Pitch Mode";
  char str5[] = " Solo Mode";
  char str6[] = " Scales Mode";
  switch (currentMenu) {
    case 0:
    lcdPrint(1, 0, 13, str);
    break;
    case 1:
    lcdPrint(1, 0, 12, str2);
    break;
    case 2:
    lcdPrint(1, 0, 13, str3);
    break;
    case 3:
    lcdPrint(1, 0, 11, str4);
    break;
    case 4:
    lcdPrint(1, 0, 10, str5);
    break;
    case 5:
    lcdPrint(1, 0, 12, str6);
    break;
  }
}

void adjustLight() {
  lcdPrint(2, 0, 0, '0');
  lcdPrint(0, 0, 13, "Press key, if");
  lcdPrint(1, 0, 16, "laser on sensor.");
  waitForKeypad();
  int lightValueLaser = getLight();
  lcdPrint(2, 0, 0, '0');
  lcdPrint(0, 0, 13, "Press key, if");
  lcdPrint(1, 0, 14, "laser blocked.");
  waitForKeypad();
  int lightValueBlocked = getLight();
  lightValueMiddle = (lightValueLaser + lightValueBlocked) / 2;
  currentMode = 0;
  printLiveMenu();
}

/**
 * Octaves only from 1 to 8 (4 = default)
 **/
void playNormal() {
  lcdPrint(2, 0, 0, '0');
  lcdPrint(0, 0, 11, "Normal Mode");
  lcdPrint(1, 11, 4, "Oc:5");
  
  char key;
  int isPressed = 0;
  int currentTone = 0;
  int tempDistance;
  while (1) {
    key = getKey();
    if (key == '*') {
      if (octave > 1) {
        octave -= 1;
        char c1 = '0' + octave;
        lcdPrint(1, 14, 1, &c1);
      }
    } else if (key == '#') {
      if (octave < 8) {
        octave += 1;
        char c1 = '0' + octave;
        lcdPrint(1, 14, 1, &c1);
      }
    } else if (key == 'B') {
      break;
    } else if (key == 'D') {
      currentMenu = 2;
      currentMode = 3; // switch to normal mode plus
      playNormalPlus();
      break;
    } else if (key == 'A') {
      switchToSeq();
      break;
    }
    tempDistance = getDistance();
    if (tempDistance > maxDistance) {
    } else if (getLight() > lightValueMiddle && isPressed == 0) {
      halftone = getToneFromDistance(tempDistance);
      isPressed = 1;
      currentTone = (octave + 1) * 12 + halftone;
      midiWrite(0x90, currentTone, 0x45);
    }
    if (getLight() < lightValueMiddle) {
      isPressed = 0;
      midiWrite(0x80, currentTone, 0x00);
    }
    delayMillis(10);
  }
  if (isPressed) {
    midiWrite(0x80, currentTone, 0x00);
  }
  currentMode = 0;
  printLiveMenu();
}

void playNormalPlus() {
  lcdPrint(2, 0, 0, '0');
  lcdPrint(0, 0, 12, "Normal Mode+");
  lcdPrint(1, 11, 4, "Oc:5");

  char key;
  int tempDistance;
  int isPressed = 0;
  int timeOfLastTone;
  int currentTone = 0;
  int lastTone;
  while (1) {
    key = getKey();
    if (key == '*') {
      if (octave > 1) {
        octave -= 1;
        char c1 = '0' + octave;
        lcdPrint(1, 14, 1, &c1);
      }
    } else if (key == '#') {
      if (octave < 8) {
        octave += 1;
        char c1 = '0' + octave;
        lcdPrint(1, 14, 1, &c1);
      }
    } else if (key == 'B') {
      break;
    } else if (key == 'D') {
      currentMenu = 3;
      currentMode = 4; // switch to pitch mode
      playPitch();
      break;
    } else if (key == 'A') {
      switchToSeq();
      break;
    }
    tempDistance = getDistance();
    if (tempDistance > maxDistance) {
    } else if (getLight() > lightValueMiddle && isPressed == 0) {
      timeOfLastTone = getMillis();
      isPressed = 1;
      currentTone = getToneFromDistance(tempDistance) + (octave + 1) * 12;
      midiWrite(0x90, currentTone, 0x45);
    } else if (isPressed == 1 && getToneFromDistance(tempDistance) + 12 * (octave + 1) != currentTone && ((getMillis() - timeOfLastTone) > 200 || getToneFromDistance(tempDistance) + 12 * (octave + 1) != lastTone) && getToneFromDistance(tempDistance) + 12 * (octave + 1) - currentTone <= 3 && getToneFromDistance(tempDistance) + 12 * (octave + 1) - currentTone >= -3) {
      // if the same tone as before is played in less than 0.2 seconds, it is skipped. Prevents unwanted tremolo, No intervals > 2 halftones allowed
      midiWrite(0x80, currentTone, 0x00);
      timeOfLastTone = getMillis();
      lastTone = currentTone;
      currentTone = getToneFromDistance(tempDistance) + 12 * (octave + 1);
      midiWrite(0x90, currentTone, 0x45);
    }
    if (getLight() < lightValueMiddle) {
      isPressed = 0;
      midiWrite(0x80, currentTone, 0x00);
    }
    delayMillis(10);
  }
  if (isPressed) {
    midiWrite(0x80, currentTone, 0x00);
  }
  currentMode = 0;
  printLiveMenu();
}

void playScales() {
  char key;

  //playing info
  int tempDistance;
  int isPressed = 0;
  int lastTone;
  int timeOfLastTone;

  //chord information
  int isMajor = 1;
  int numNotes = 1;
  int baseKey = 48; //C
  int currentTone = 0;
  int baseTone = 0;

  //octave information
  int minOctave = 48;
  int maxOctave = 60;

  printScales(isMajor, baseKey, numNotes);

  while(1) {
    key = getKey();
    if (key == '8') {
      //switch from major to minor or vice versa
      isMajor = !isMajor;
      printScales(isMajor, baseKey, numNotes);

    } else if (key == '9') {
      //switch base key
      baseKey += 1;
      if (baseKey == maxOctave) baseKey = minOctave;
      printScales(isMajor, baseKey, numNotes);

    } else if (key == 'B') {
      break;

    } else if (key == 'C') {
      //play more notes
      numNotes = numNotes + 1;
      if (numNotes == 6) numNotes = 1;  //maximum of 5 notes
      printScales(isMajor, baseKey, numNotes);

    } else if (key == 'D') {
      //switch to normal mode
      currentMenu = 1;
      currentMode = 1;
      playNormal();
      break;
    } else if (key == '*') {
      //change octave down
      if (baseKey > 0) {
        baseKey -= 12;
        minOctave -= 12;
        maxOctave -= 12;
        printScales(isMajor, baseKey, numNotes);
      }

    } else if (key == '#') {
      //change octave up
      if (baseKey < 96) {
        baseKey += 12;
        minOctave += 12;
        maxOctave += 12;
        printScales(isMajor, baseKey, numNotes);
      }
    } else if (key == '7') {
      //switch base key
      baseKey -= 1;
      if (baseKey == minOctave - 1) baseKey = maxOctave - 1;
      printScales(isMajor, baseKey, numNotes);

    } else if (key == '5') {
      baseTone = !baseTone;
      lcdPrint(2, 0, 0, '0');
      if (baseTone) lcdPrint(0, 0, 12, "Base Tone On");
      else lcdPrint(0, 0, 13, "Base Tone Off");
      delayMillis(1000);
      printScales(isMajor, baseKey, numNotes);

    } else if (key == 'A') {
      switchToSeq();
      break;
    }

    tempDistance = getDistance();
    if (tempDistance > maxDistance) {
    } else if (getLight() > lightValueMiddle && isPressed == 0) {
      
      timeOfLastTone = getMillis();
      isPressed = 1;
      //get note
      halftone = getToneFromDistanceScales(tempDistance);
      currentTone = getScaleTone(isMajor, baseKey, halftone); //in scale
      //write to midi
      playMidiScales(currentTone, numNotes, isMajor, baseKey);
      if (baseTone) playBaseTone();
    }

    if (getLight() < lightValueMiddle) {
      isPressed = 0;
      clearMidi();  //cancel midi
    }
    
    delayMillis(10);
  }
  if (isPressed) {
      clearMidi();  //cancel midi
  }
  currentMode = 0;
  printLiveMenu();
}

void printScales(int isMajor, int base, int numNotes) {
  char* key = getKeyFromBase(base);
  char* type;
  if (isMajor) type = "Major";
  else type = "Minor";

  //octave
  int octNum = base / 12;
  char oct = '0' + octNum;

  lcdPrint(2, 0, 0, '0'); //clear lcd
  //first row
  lcdPrint(0, 0, 11, "Scales Mode");
  lcdPrint(0, 13, 2, "O:");
  lcdPrint(0, 15, 1, &oct);
  //second row
  lcdPrint(1, 0, 4, "Key:");
  lcdPrint(1, 5, 2, key);
  lcdPrint(1, 8, 5, type);

  //amount of notes
  char num = '0' + numNotes;
  lcdPrint(1, 15, 1, &num);
}

void playPitch() {
  lcdPrint(2, 0, 0, '0');
  lcdPrint(0, 0, 10, "Pitch Mode");
  lcdPrint(1, 11, 4, "Oc:4");
  
  char key;
  int isPressed = 0;
  int currentTone = 0;
  int tempDistance;
  int pitchVal;
  int toneDistance;
  while (1) {
    key = getKey();
    if (key == '*') {
      if (octave > 1) {
        octave -= 1;
        char c1 = '0' + octave;
        lcdPrint(1, 14, 1, &c1);
      }
    } else if (key == '#') {
      if (octave < 8) {
        octave += 1;
        char c1 = '0' + octave;
        lcdPrint(1, 14, 1, &c1);
      }
    } else if (key == 'B') {
      break;
    } else if (key == 'D') {
      currentMenu = 4;
      currentMenu = 5;
      playSolo();
      break;
    } else if (key == 'A') {
      switchToSeq();
      break;
    }
    tempDistance = getDistance();
    if (tempDistance > maxDistance) {
    } else if (getLight() > lightValueMiddle && isPressed == 0) {
      isPressed = 1;
      currentTone = (octave + 1) * 12 + getToneFromDistance(tempDistance);
      toneDistance = tempDistance;
      midiWrite(0x90, currentTone, 0x45);
    } else if (isPressed == 1) {
      // Map distance (-10cm to 10cm) to pitchvalue (0-127)
      if (tempDistance - toneDistance <= -10) {
        pitchVal = 0;
      } else if (tempDistance - toneDistance >= 10) {
        pitchVal = 127;
      } else {
        pitchVal = (tempDistance - toneDistance) * 6.4 + 64;
      }
      midiWrite(0xE0, 0x00, pitchVal);  //Set Pitch
    }
    if (getLight() < lightValueMiddle) {
      isPressed = 0;
      midiWrite(0xE0, 0x00, 0x40); //Pitch off
      midiWrite(0x80, currentTone, 0x00);
    }
    delayMillis(10);
  }
  if (isPressed) {
    midiWrite(0xE0, 0x00, 0x40);
    midiWrite(0x80, currentTone, 0x00);
  }
  currentMode = 0;
  printLiveMenu();
}

void playSolo() {
  char key;

  //playing info
  int tempDistance;
  int isPressed = 0;
  int timeOfLastTone;
  int lastTone;

  //tone info
  int isMajor = 1;
  int baseKey = 48; //C
  int currentTone = 0;
  int baseTone = 0;

  //octave info
  int minOctave = 48;
  int maxOctave = 60;

  printSolo(isMajor, baseKey);

  while (1) {
    key = getKey();
    if (key == '*') {
      //change octave down
      if (baseKey > 0) {
        baseKey -= 12;
        minOctave -= 12;
        maxOctave -= 12;
        printSolo(isMajor, baseKey);
      }

    } else if (key == '#') {
    //change octave up
      if (baseKey < 96) {
        baseKey += 12;
        minOctave += 12;
        maxOctave += 12;
        printSolo(isMajor, baseKey);
      }

    } else if (key == 'B') {
    break;

    } else if (key == 'D') {
      currentMenu = 5;
      currentMode = 6; // switch to scales mode
      playScales();
      break;
    } else if (key == '8') {
      //switch from major to minor or vice versa
      isMajor = !isMajor;
      printSolo(isMajor, baseKey);

    } else if (key == '7') {
      //switch base key
      baseKey -= 1;
      if (baseKey == minOctave - 1) baseKey = maxOctave - 1;
      printSolo(isMajor, baseKey);

    } else if (key == '9') {
      //switch base key
      baseKey += 1;
      if (baseKey == maxOctave) baseKey = minOctave;
      printSolo(isMajor, baseKey);

    } else if (key == 'A') {
      switchToSeq();
      break;
    }

  tempDistance = getDistance();
  if (tempDistance > maxDistance) {
    //nothing
  } else if (getLight() > lightValueMiddle && isPressed == 0) {

    timeOfLastTone = getMillis();
    isPressed = 1;
    //get note
    halftone = getToneFromDistanceScales(tempDistance);
    currentTone = getScaleTone(isMajor, baseKey, halftone);
    playMidiScales(currentTone, 1, isMajor, baseKey);

  } else if (isPressed && getScaleTone(isMajor, baseKey, getToneFromDistanceScales(getDistance())) != currentTone && (getMillis() - timeOfLastTone) > 200) {
    // if the same tone as before is played in less than 0.2 seconds, it is skipped. Prevents unwanted tremolo
    clearMidi();
    timeOfLastTone = getMillis();
    lastTone = currentTone;
    //new tone
    halftone = getToneFromDistanceScales(tempDistance);
    currentTone = getScaleTone(isMajor, baseKey, halftone);
    playMidiScales(currentTone, 1, isMajor, baseKey);
  }

  if (getLight() < lightValueMiddle) {
    isPressed = 0;
    clearMidi();
  }

  delayMillis(10);
}

if (isPressed) {
  clearMidi();
}

currentMode = 0;
printLiveMenu();
}

void printSolo(isMajor, baseKey) {
  char* key = getKeyFromBase(baseKey);
  char* type;
  if (isMajor) type = "Major";
  else type = "Minor";

  //octave
  int octNum = baseKey / 12;
  char oct = '0' + octNum;

  lcdPrint(2, 0, 0, '0'); //clear lcd
  //first row
  lcdPrint(0, 0, 9, "Solo Mode");
  lcdPrint(0, 13, 2, "O:");
  lcdPrint(0, 15, 1, &oct);
  //second row
  lcdPrint(1, 0, 4, "Key:");
  lcdPrint(1, 5, 2, key);
  lcdPrint(1, 8, 5, type);
}

/**
 * Up to 4cm no tone. In steps of 3cm one halftone up to 40cm. (12 halftones)
**/
int getToneFromDistance(int cm) {
  return (cm - 4) / 3;
}

/**
 * Up to 4cm no tone. In steps of 5cm one step up to 40cm. (8 steps)
 */
int getToneFromDistanceScales(int cm) {
  return (cm - 4) / 5;
}

int getLightValueMiddle() {
  return lightValueMiddle;
}
