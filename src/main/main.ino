/**
 * Laser OS 1.0 by Maximilian Barth, Philipp Weber, Simon Laube
 **/

#define I2C // only define if the current laser instrument has a Liquid Crystal IC2 version
extern "C"{
  #include "sequencer.h"
  #include "liveMode.h"
  #include "scales.h"
  #include "midiPlayer.h"
}

#ifdef I2C
#include <LiquidCrystal_I2C.h>
#else
#include <LiquidCrystal.h>
#endif

#include <Keypad.h>

const byte ROWS = 4; //four keypad rows
const byte COLS = 4; //four keypad columns
char keys[ROWS][COLS] = { {'1','2','3','A'}, {'4','5','6','B'}, {'7','8','9','C'}, {'*','0','#','D'} };
char key = 'x';

byte rowPins[ROWS] = {30,31,32,33}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {34,35,36,37}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

bool isLiveMode; // false = sequencer

const int echoPin = 50;
const int trigPin = 51;

long duration;
int distance;

int lightPin = A0;
int lightValue = 0;

#ifdef I2C
LiquidCrystal_I2C lcd(0x27,16,2);
#else
LiquidCrystal lcd(12, 11, 5, 4, 3, 2); 
int Contrast = 6;
#endif

void setup() {
  #ifdef I2C
  lcd.init();
  lcd.backlight();
  #else
  lcd.begin(16,2);
  analogWrite(6,Contrast);
  #endif
  
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  pinMode(lightPin, INPUT);
    
  Serial.begin(31250);
  
  isLiveMode = true;
  initSequencer(&midiWrite, &delayMillis, &lcdPrint, &getKey, &getMillis, &waitForKeypad, &getDistance, &getLight, &changeMode);
  initMidiPlayer(&midiWrite);
  initLiveMode(&midiWrite, &delayMillis, &lcdPrint, &getKey, &getMillis, &waitForKeypad, &getDistance, &getLight, &changeMode);
}

void loop() {
  getCurrentKeypadInput();    //read keypad
  delay(40);
}

/**
 * reads the current input from the keypad
 * acts accordingly
 * used in main loop above
 */
void getCurrentKeypadInput() {
  
  key = keypad.getKey();
  
  switch (key) {
    case NO_KEY:
      break;
    case 'A':
      changeMode();
      break;
    default:
      handleInput(key);
      break;
  }
}

void changeMode() {
  isLiveMode = !isLiveMode;
  lcd.clear();
  if (!isLiveMode) {
    char str[] = " Sequencer";
    lcdPrint(0, 0, 10, str);
    delay(500);
    returnToSequencer();
  } else {
    char str[] = " Live Mode";
    lcdPrint(0, 0, 10, str);
    delay(500);
    returnToLiveMode();
  }
}

void handleInput(char key) {
  if (isLiveMode) {
    handleLiveKeyInput(key);
  } else {
    handleSeqKeyInput(key);
  }
}

void midiWrite(int cmd, int pitch, int velocity) {
  Serial.write(cmd);
  Serial.write(pitch);
  Serial.write(velocity);
}

void delayMillis(int mil) {
  delay(mil);
}

void lcdPrint(int row, int col, int length, char* text) {
  if (row == 2) {
    lcd.clear();
    return;
  }
  int x;
  int i = 0;
  for (x = col; x < length + col; x++) {
    lcd.setCursor(x, row);
    lcd.print(text[i]);
    i++;
  }
}

/**
 * waits for user input from keypad and returns pressed char
 * used in sequencer to get input from keypad
 */
char waitForKeypad() {
  char c = NULL;

  while (c == NULL) {

    key = keypad.getKey();
    switch (key) {
    case NO_KEY:
      break;
    case '1':
      c = '1';
      break;
    case '2':
      c = '2';
      break;
    case '3':
      c = '3';
      break;
    case 'A':
      c = 'A';
      break;
    case '4':
      c = '4';
      break;
    case '5':
      c = '5';
      break;
    case '6':
      c = '6';
      break;
    case 'B':
      c = 'B';
      break;
    case '7':
      c = '7';
      break;
    case '8':
      c = '8';
      break;
    case '9':
      c = '9';
      break;
    case 'C':
      c = 'C';
      break;
    case '*':
      c = '*';
      break;
    case '0':
      c = '0';
      break;
    case '#':
      c = '#';
      break;
    case 'D':
      c = 'D';
      break;
    }
  }

  return c;
}

char* getKey() {
  return keypad.getKey();
}

int getMillis() {
  return millis();
}

int getLight() {
  return analogRead(lightPin);
}

int getDistance() {
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH); // Reads the echoPin, returns the sound wave travel time in microseconds
  return duration * 0.034 / 2; // Speed of sound wave divided by 2
}
